package com.epam.rd.java.basic.task8.entities;

import com.epam.rd.java.basic.task8.xmladapters.FieldCmAdapter;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(propOrder={"stemColour", "leafColour", "aveLenFlower"})
public class VisualParameters {

    private String stemColour;
    private String leafColour;

    @XmlJavaTypeAdapter(FieldCmAdapter.class)
    private String aveLenFlower;


    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getAveLenFlower() {
        return aveLenFlower;
    }

    @XmlTransient
    public void setAveLenFlower(String aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }


    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}

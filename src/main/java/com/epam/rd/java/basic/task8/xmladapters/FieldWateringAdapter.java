package com.epam.rd.java.basic.task8.xmladapters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FieldWateringAdapter extends XmlAdapter<FieldWateringAdapter.adaptedWatering, Integer> {

    @Override
    public Integer unmarshal(adaptedWatering v) throws Exception {
        return v.value;
    }

    @Override
    public adaptedWatering marshal(Integer v) throws Exception {
        adaptedWatering amf = new adaptedWatering();
        amf.measure = "mlPerWeek";
        amf.value = v;
        return amf;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class adaptedWatering {
        @XmlAttribute
        public String measure;

        @XmlValue
        public Integer value;
    }

}
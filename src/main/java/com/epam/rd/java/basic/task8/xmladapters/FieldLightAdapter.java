package com.epam.rd.java.basic.task8.xmladapters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FieldLightAdapter extends XmlAdapter<FieldLightAdapter.AdapterLightField, String> {

    @Override
    public String unmarshal(AdapterLightField v) throws Exception {
        return v.lightRequiring;
    }

    @Override
    public AdapterLightField marshal(String v) throws Exception {
        AdapterLightField amf = new AdapterLightField();
        amf.lightRequiring = v;
        return amf;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AdapterLightField {
        @XmlAttribute
        public String lightRequiring;
    }

}
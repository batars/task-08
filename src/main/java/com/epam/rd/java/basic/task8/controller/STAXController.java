package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    private static Flowers flowers;
    private static Flower flower;
    private static GrowingTips growingTips;
    private static VisualParameters visualParameters;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() throws IOException, XMLStreamException {
        validateXml();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "flowers":
                        flowers = new Flowers();
                        break;
                    case "flower":
                        flower = new Flower();
                        break;
                    case "growingTips":
                        nextEvent = reader.nextEvent();
                        growingTips = new GrowingTips();
                        break;
                    case "visualParameters":
                        nextEvent = reader.nextEvent();
                        visualParameters = new VisualParameters();
                        break;
                    case "name":
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case "soil":
                        nextEvent = reader.nextEvent();
                        flower.setSoil(nextEvent.asCharacters().getData());
                        break;
                    case "origin":
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case "multiplying":
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(nextEvent.asCharacters().getData());
                        break;
                    case "stemColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case "leafColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case "aveLenFlower":
                        nextEvent = reader.nextEvent();
                        visualParameters.setAveLenFlower(nextEvent.asCharacters().getData());
                        break;
                    case "tempreture":
                        nextEvent = reader.nextEvent();
                        growingTips.setTempreture(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "watering":
                        nextEvent = reader.nextEvent();
                        growingTips.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "lighting":
                        growingTips.setLighting(startElement.getAttributes().next().getValue());
                        break;
                }
            }
            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("flower")) {
                    flowers.getFlowers().add(flower);
                }

                if (endElement.getName().getLocalPart().equals("growingTips")) {
                    flower.setGrowingTips(growingTips);
                }

                if (endElement.getName().getLocalPart().equals("visualParameters")) {
                    flower.setVisualParameters(visualParameters);
                }
            }
        }

        return flowers;
    }

    private void validateXml() throws XMLStreamException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource("input.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFileName));
        } catch(Exception ex)        {
            throw new XMLStreamException("Invalid xml",ex);
        }
    }
}
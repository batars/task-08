
package com.epam.rd.java.basic.task8.entities;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name="flowers")
public class Flowers {

    @XmlElement(name = "flower")
    private List<Flower> flowers;

    public Flowers() {
        flowers = new ArrayList<>();
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void saveToXML(String fileName) throws JAXBException, FileNotFoundException {
        JAXBContext contextObj = JAXBContext.newInstance(Flowers.class);
        Marshaller marshallerObj = contextObj.createMarshaller();
        marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerObj.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.nure.ua input.xsd");
        marshallerObj.marshal(this, new FileOutputStream(fileName));
    }

    public void sortByName() {
        flowers = flowers.stream().sorted(Comparator.comparing(Flower::getName)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flowers=" + flowers +
                '}';
    }

    public void sortBySoil() {
        flowers = flowers.stream().sorted(Comparator.comparing(Flower::getSoil)).collect(Collectors.toList());
    }

    public void sortByCesium() {
        flowers = flowers.stream().sorted(Comparator.comparing(f -> f.getGrowingTips().getTempreture())).collect(Collectors.toList());
    }
}

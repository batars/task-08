package com.epam.rd.java.basic.task8.entities;

import com.epam.rd.java.basic.task8.xmladapters.FieldCelciusAdapter;
import com.epam.rd.java.basic.task8.xmladapters.FieldLightAdapter;
import com.epam.rd.java.basic.task8.xmladapters.FieldWateringAdapter;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(propOrder={"tempreture", "lighting", "watering"})
public class GrowingTips {

    @XmlJavaTypeAdapter(FieldCelciusAdapter.class)
    private Integer tempreture;

    @XmlJavaTypeAdapter(FieldLightAdapter.class)
    private String lighting;

    @XmlJavaTypeAdapter(FieldWateringAdapter.class)
    private Integer watering;

    public Integer getTempreture() {
        return tempreture;
    }

    @XmlTransient
    public void setTempreture(Integer tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    @XmlTransient
    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public Integer getWatering() {
        return watering;
    }

    @XmlTransient
    public void setWatering(Integer watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                '}';
    }
}

package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Flowers;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers domFlowers = domController.getFlowers();

		// sort (case 1)
		domFlowers.sortByName();
		
		// save
		String outputXmlFile = "output.dom.xml";
		domFlowers.saveToXML(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		Flowers saxFlowers = saxController.getFlowers();

		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		saxFlowers.sortBySoil();
		
		// save
		outputXmlFile = "output.sax.xml";
		saxFlowers.saveToXML(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		Flowers staxFlowers = staxController.getFlowers();
		
		// sort  (case 3)
		staxFlowers.sortByCesium();
		
		// save
		outputXmlFile = "output.stax.xml";
		staxFlowers.saveToXML(outputXmlFile);
	}

}

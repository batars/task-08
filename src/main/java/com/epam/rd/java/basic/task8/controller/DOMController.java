package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        dbf.setFeature("http://xml.org/sax/features/validation", true);
        dbf.setFeature("http://apache.org/xml/features/validation/schema", true);

        validateXml();

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(xmlFileName);

        Element root = doc.getDocumentElement();

        Flowers flowers = new Flowers();

        NodeList flowerNodes = root.getElementsByTagName("flower");

        for (int j = 0; j < flowerNodes.getLength(); j++) {
            Flower flower = getFlower(flowerNodes.item(j));
            flowers.getFlowers().add(flower);
        }

        return flowers;
    }

    private Flower getFlower(Node node) {
        Flower flower = new Flower();

        NodeList children = node.getChildNodes();

        for (int j = 0; j < children.getLength(); j++) {
            Node currentNode = children.item(j);
            switch (currentNode.getNodeName()) {
                case "name":
                    flower.setName(currentNode.getFirstChild().getNodeValue());
                    break;
                case "soil":
                    flower.setSoil(currentNode.getFirstChild().getNodeValue());
                    break;
                case "origin":
                    flower.setOrigin(currentNode.getFirstChild().getNodeValue());
                    break;
                case "multiplying":
                    flower.setMultiplying(currentNode.getFirstChild().getNodeValue());
                    break;
                case "growingTips":
                    flower.setGrowingTips(getGrowingTips(currentNode));
                    break;
                case "visualParameters":
                    flower.setVisualParameters(getVisualParameters(currentNode));
                    break;
            }
        }

        return flower;
    }

    private GrowingTips getGrowingTips(Node node) {
        GrowingTips tips = new GrowingTips();

        NodeList children = node.getChildNodes();

        for (int j = 0; j < children.getLength(); j++) {
            Node currentNode = children.item(j);
            switch (currentNode.getNodeName()) {
                case "tempreture":
                    tips.setTempreture(Integer.parseInt(currentNode.getFirstChild().getNodeValue()));
                    break;
                case "lighting":
                    tips.setLighting(currentNode.getAttributes().item(0).getNodeValue());
                    break;
                case "watering":
                    tips.setWatering(Integer.parseInt(currentNode.getFirstChild().getNodeValue()));
                    break;
            }
        }

        return tips;
    }

    private VisualParameters getVisualParameters(Node node) {
        VisualParameters visualParameters = new VisualParameters();

        NodeList children = node.getChildNodes();
        for (int j = 0; j < children.getLength(); j++) {
            Node currentNode = children.item(j);
            switch (currentNode.getNodeName()) {
                case "stemColour":
                    visualParameters.setStemColour(currentNode.getFirstChild().getNodeValue());
                    break;
                case "leafColour":
                    visualParameters.setLeafColour(currentNode.getFirstChild().getNodeValue());
                    break;
                case "aveLenFlower":
                    visualParameters.setAveLenFlower(currentNode.getFirstChild().getNodeValue());
                    break;
            }
        }

        return visualParameters;
    }

    private void validateXml() throws SAXException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource("input.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFileName));
        } catch(Exception ex)        {
            throw new SAXException("Invalid xml",ex);
        }
    }
}

package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() throws IOException, SAXException, ParserConfigurationException {
        validateXml();

        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);

        factory.setFeature("http://xml.org/sax/features/validation", true);
        factory.setFeature("http://apache.org/xml/features/validation/schema", true);

        SAXParser parser = factory.newSAXParser();

        parser.parse(xmlFileName, new SaxHandler());

        return flowers;
    }


    private  Flowers flowers;
    private  Flower flower;
    private  GrowingTips growingTips;
    private  VisualParameters visualParameters;

    private class SaxHandler extends DefaultHandler {

        private String currentName;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            currentName = localName;

            switch (localName) {
                case "flowers":
                    flowers = new Flowers();
                    break;
                case "flower":
                    flower = new Flower();
                    break;
                case "growingTips":
                    growingTips = new GrowingTips();
                    break;
                case "visualParameters":
                    visualParameters = new VisualParameters();
                    break;
                case "lighting":
                    growingTips.setLighting(attributes.getValue(0));
                    break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String content =
                    new String(ch, start, length).trim();

            if (content.isEmpty()) {
                return;
            }

            if ("name".equals(currentName)) {
                flower.setName(content);
            }

            if ("soil".equals(currentName)) {
                flower.setSoil(content);
            }

            if ("origin".equals(currentName)) {
                flower.setOrigin(content);
            }

            if ("multiplying".equals(currentName)) {
                flower.setMultiplying(content);
            }

            if ("stemColour".equals(currentName)) {
                visualParameters.setStemColour(content);
            }

            if ("leafColour".equals(currentName)) {
                visualParameters.setLeafColour(content);
            }

            if ("aveLenFlower".equals(currentName)) {
                visualParameters.setAveLenFlower(content);
            }

            if ("tempreture".equals(currentName)) {
                growingTips.setTempreture(Integer.parseInt(content));
            }

            if ("watering".equals(currentName)) {
                growingTips.setWatering(Integer.parseInt(content));
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch (localName) {
                case "flower":
                    flowers.getFlowers().add(flower);
                    break;
                case "growingTips":
                    flower.setGrowingTips(growingTips);
                    break;
                case "visualParameters":
                    flower.setVisualParameters(visualParameters);
                    break;
            }
        }
    }

    private void validateXml() throws SAXException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource("input.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFileName));
        } catch(Exception ex)        {
            throw new SAXException("Invalid xml",ex);
        }
    }
}
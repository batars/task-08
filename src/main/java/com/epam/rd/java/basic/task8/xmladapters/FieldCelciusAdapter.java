package com.epam.rd.java.basic.task8.xmladapters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FieldCelciusAdapter extends XmlAdapter<FieldCelciusAdapter.adaptedCelsius, Integer> {

    @Override
    public Integer unmarshal(adaptedCelsius v) throws Exception {
        return v.value;
    }

    @Override
    public adaptedCelsius marshal(Integer v) throws Exception {
        adaptedCelsius amf = new adaptedCelsius();
        amf.measure = "celcius";
        amf.value = v;
        return amf;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class adaptedCelsius {
        @XmlAttribute
        public String measure;

        @XmlValue
        public Integer value;
    }

}
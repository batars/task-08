package com.epam.rd.java.basic.task8.xmladapters;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FieldCmAdapter extends XmlAdapter<FieldCmAdapter.AdaptedMyField, String> {

    @Override
    public String unmarshal(AdaptedMyField v) throws Exception {
        return v.value;
    }

    @Override
    public AdaptedMyField marshal(String v) throws Exception {
        AdaptedMyField amf = new AdaptedMyField();
        amf.measure = "cm";
        amf.value = v;
        return amf;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AdaptedMyField {
        @XmlAttribute
        public String measure;

        @XmlValue
        public String value;
    }

}